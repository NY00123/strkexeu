# Desert Strike: Return to the Gulf and Jungle Strike DOS EXE unpacking

This archive covers a standalone unpacker for Desert/Jungle Strike DOS EXEs,
along with a disassembly of the original loader code.

## Background

With the possible exception of an early demo version of Desert Strike,
chances are the DESERT.EXE/JUNGLE.EXE file doesn't consist of actual game code.
Instead, the game EXE is found in the DAT file, albeit in a packed form.

In such a case, DESERT.EXE/JUNGLE.EXE is a loader, which is unpacking
the game EXE from the DAT file to memory, preparing it for execution
and then transferring control to its unpacked code.

## List of contents

- strikeld.asm: A disassembly of the original Desert/Jungle Strike DOS EXE
loader code, including a few variations controlled by macros.

- strkexeu: A standalone program that can be used for unpacking
the Desert/Jungle Strike DOS EXE files, as present in DAT files.
It's provided as a one-file reference implementation.

## Terms of use

strkexeu is available under the terms of the 3-clause BSD license.
See the beginning of the file strkexeu.c for more details.

## Building strkexeu from the source code

strkexeu.c should be compatible with multiple C compilers. It's the only
source in use for the standalone unpacker, with no dependency on any
library, other than C standard library functions.

Note that in case stdbool and/or stdint definitions aren't provided
by the compiler, e.g., due to not building the code as C99 or compatible,
you might have to adjust locally-defined typedefs, say for the bool enum.

## Building strikeld.asm

strikeld.asm can be assembled with Turbo Assembler 3.0, and then linked with
Turbo Linker 3.01. Exactly one of the macros DESERT and JUNGLE must be defined.
An additional macro, CD, can optionally be defined for adding a check
that the loader EXE resides on a CD.

If the aforementioned versions of Turbo Assembler and Turbo Linker are used,
the output EXEs are expected to be virtually identical to the originals from
the 90s. Main difference is expected in the DOS EXE header.
Basically, the minimum amount of paragraphs of memory allocated in addition
to code size will be smaller by 500h (1536) than in the original matching EXE.
This amount is represented by the little-endian 16-bit integer at offset 0Ah.

-Yoav N.
