IDEAL
MODEL MEDIUM

IFDEF DESERT
EXE_HEADER_AND_RELOC_BUFFER_SIZE = 800h
ELSEIFDEF JUNGLE
EXE_HEADER_AND_RELOC_BUFFER_SIZE = 8000h
ENDIF

FILE_CHUNK_SIZE = 8000h

codeseg loader

start:
	assume cs:loader
	push	cs
	pop	ds
	assume ds:loader
	mov	[psp_seg], es
	mov	es, [es:2Ch] ; Environment segment from the PSP
	mov	di, 0
	mov	al, 0
	mov	cx, 0FFFFh

skip_environment_variable:
	repne scasb
	cmp	[es:di], al
	jnz	skip_environment_variable
	add	di, 2 ; After this, es:di+1 should point at a path to EXE

IFDEF CD

	push	es
	push	di
	; Get the no. of CD-ROM drive letters in bx, and the first one in cx
	mov	ax, 1500h
	mov	bx, 0
	int	2Fh
	pop	di
	pop	es
	and	bx, bx ; No. of CD-ROM drive letters
	jz	cd_check_fail
	mov	al, [es:di+1] ; Grab drive letter from path to the EXE
	sub	al, 'A'
	cmp	al, cl ; cx: Starting drive letter, beginning from 0
	jnb	cd_check_pass

	; Write error message and then exit with code 1
cd_check_fail:
	mov	dx, offset cd_errormsg
	mov	cx, offset cd_check_pass
	sub	cx, dx
	mov	bx, 1
	mov	ah, 40h
	int	21h
	mov	ax, 4C01h
	int	21h

cd_errormsg:	db	'Not running from CD-ROM.'

cd_check_pass:

ENDIF ; CD

	; Append DAT filename with path to the error message in the loader EXE.
	; Initially, the EXE name will get temporarily appended instead.
	; As a little reminder, es:di+1 should point at a path to the EXE.
	mov	si, offset filewithpath

loop_append_exe_path:
	inc	di
	mov	al, [es:di]
	cmp	al, 0
	jz	remove_up_to_backslash
	mov	[si], al
	inc	si
	jmp	short loop_append_exe_path

remove_up_to_backslash:
	dec	si
	mov	al, 0
	xchg	al, [si]
	cmp	al, '\'
	jnz	remove_up_to_backslash
	; Append the DAT file to the message with the path, instead of the EXE
	push	ds
	pop	es
	mov	di, si
	mov	si, offset prefixedfile
	mov	cx, errormsg - prefixedfile - 1
	repe movsb
	mov	[path_end_offset], di
	; Make another copy of this program in memory
	mov	es, [psp_seg]
	mov	ax, offset end_of_data
	push	ax
	mov	cl, 4
	shr	ax, cl
	not	ax
	add	ax, [es:2] ; Add segment of the first byte after program memory
	mov	es, ax
	sub	di, di
	sub	si, si
	pop	cx
	repe movsb
	; Continue execution of this program from the new copy
	push	es
	pop	ds
	push	es
	mov	ax, offset next_start
	push	ax
	retf


next_start:
	; Set ss:sp to point at a location which is 1..16 bytes
	; after end_of_data, with the offset being end_of_data - tree.
	; What this practically means is that ss:0000 will refer to a location
	; residing 1..16 bytes after the beginning of the tree buffer.
	; A write to the 1024th byte in the segment will technically lead
	; to an overflow into exe_header_and_reloc, but exe_header_and_reloc
	; is used only after the last access to the tree.
	mov	bp, offset end_of_data
	mov	ax, offset tree
	sub	bp, ax
	mov	cl, 4
	shr	ax, cl
	inc	ax
	mov	bx, cs
	add	ax, bx
	mov	ss, ax
	mov	sp, bp
	; Open data file for reading
	mov	dx, offset filewithpath
	mov	ax, 3D00h
	int	21h
	jnb	file_opened
	jmp	abort

file_opened:
	; Read the first 32 bytes, including an offset to the packed EXE
	mov	[ds:file_handle], ax
	mov	bx, ax
	mov	ax, ds
	mov	dx, offset dat_header
	mov	cx, 20h
	call	read_from_file
	; Seek file pointer to the beginning of the packed EXE
	mov	cx, [word dat_header+1Ah]
	mov	dx, [word dat_header+18h]
	mov	ax, 4200h
	int	21h
	jnb	header_was_read
	jmp	abort

header_was_read:
	; Unpack the EXE to the segment referenced by orig_seg_ptr
	mov	ax, cs
	sub	ax, FILE_CHUNK_SIZE/16+1
	mov	[ds:file_chunk_seg_ptr], ax
	call	process_data

	; Close file
	mov	bx, [ds:file_handle]
	mov	ax, 3E00h
	int	21h

	; Go through the unpacked EXE, beginning by copying the first
	; EXE_HEADER_AND_RELOC_BUFFER_SIZE bytes of it to exe_header_and_reloc.
	; This should include the EXE header and relocation table, assuming
	; that EXE_HEADER_AND_RELOC_BUFFER_SIZE is sufficiently large.
	mov	bp, [ds:orig_seg_ptr]
	push	ds
	push	ds
	pop	es
	mov	di, offset exe_header_and_reloc
	mov	ds, bp
	sub	si, si
	mov	cx, EXE_HEADER_AND_RELOC_BUFFER_SIZE/2
	repe movsw
	; Move the EXE's binary code that follows the relocation table,
	; effectively removing the header and relocation table from
	; the copy beginning at the segment [ds:orig_seg_ptr].
	; The EXE is moved page-by-page, so if there's additional data
	; following the last page, it gets discarded, for most.
	; An exception to this is that an additional full page
	; (512 bytes) is mistakenly moved.
	pop	ds
	push	ds
	mov	bx, bp
	mov	ax, [word exe_header_and_reloc+8] ; Header size in paragraphs
	add	ax, bp
	mov	dx, [word exe_header_and_reloc+4] ; No. of pages

move_exe_page:
	mov	ds, ax
	mov	es, bx
	sub	si, si
	sub	di, di
	mov	cx, 100h
	repe movsw
	add	ax, 20h
	add	bx, 20h
	dec	dx
	jnz	move_exe_page
	; Moving the amount of bytes given by the last page's
	; size, albeit one more full page (512 byte) is
	; mistakenly moved right beforehand.
	mov	cx, [word cs:exe_header_and_reloc+2] ; Size of last page
	repe movsb
	pop	ds
	mov	cx, [word exe_header_and_reloc+6] ; No. of relocation entries
	jcxz	relocations_done
	mov	si, [word exe_header_and_reloc+18h] ; Relocation table offset
	add	si, offset exe_header_and_reloc

handle_relocation_entry:
	lodsw
	mov	bx, ax
	lodsw
	add	ax, bp
	mov	es, ax
	add	[es:bx], bp ; Patch the segment in the unpacked EXE image
	loop	handle_relocation_entry

relocations_done:
	; Last updates to registers, followed by execution
	; of the unpacked and patched program
	mov	ax, [word exe_header_and_reloc+0Eh]
	add	ax, bp
	mov	ss, ax
	mov	sp, [word exe_header_and_reloc+10h]
	mov	ax, [word exe_header_and_reloc+16h]
	add	ax, bp
	push	ax
	mov	ax, [word exe_header_and_reloc+14h]
	push	ax
	mov	ax, [ds:psp_seg]
	mov	ds, ax
	mov	es, ax
	retf

	; Unpack the EXE present in the DAT file, reading it chunk-by-chunk
process_data:
	push	ds
	mov	es, [ds:orig_seg_ptr]
	sub	di, di
	call	read_chunk
	call	process_first_chunk
	jnb	done_processing

keep_processing:
	call	read_chunk
	call	process_next_chunk
	jb	keep_processing

done_processing:
	pop	ds
	retn


read_chunk:
	push	es
	push	di
	push	cs
	pop	ds
	mov	bx, [file_handle]
	mov	ax, [file_chunk_seg_ptr]
	sub	dx, dx
	mov	cx, FILE_CHUNK_SIZE
	call	read_from_file
	mov	ds, [file_chunk_seg_ptr]
	sub	si, si
	pop	di
	pop	es
	retn

; A few variables used while processing data from file
nodecount	db 0
child1		db 0
bytestowrite	dw 0
singletonmark	db 0
node		dw 0
stageptr	dw 0
treestepptr	dw 0

process_first_chunk:
	mov	[cs:stageptr], offset stage_get_nodecount
	mov	[cs:treestepptr], offset treestep_process_cmd
	; Fallthrough

process_next_chunk:
	mov	bx, [cs:stageptr]
	mov	dx, [cs:treestepptr]

process_byte:
	lodsb
	call	bx
	loop	process_byte
	mov	[cs:stageptr], bx
	mov	[cs:treestepptr], dx
	stc ; Ensure that execution of process_data continues
	retn

; List of differing stages of processing input data. There are
; separate steps for processing decoded bytes from trees.

stage_finishscan:
	; Return right to process_data,
	; skipping over process_first_chunk/process_next_chunk,
	; and use clc to finish the execution of process_data.
	pop	ax
	clc
	retn

stage_get_nodecount:
	mov	[cs:nodecount], al
	mov	bx, offset stage_init_forest
	retn

stage_init_forest:
	cmp	[cs:nodecount], 0
	jnz	nodecount_is_nonzero
	mov	bx, offset stage_process_byte_as_is
	retn

nodecount_is_nonzero:
	mov	[cs:singletonmark], al
	mov	bp, 0
	mov	ax, 0

	; Initialize the tree buffer's nodes to represent what looks
	; like leaves. As written earlier, the 1024th byte of the tree segment
	; (from the last node) technically overflows into exe_header_and_reloc,
	; but exe_header_and_reloc is used only after fully unpacking the EXE.
loop_init_forest:
	mov	[bp+0], ax
	add	bp, 4
	inc	al
	jnz	loop_init_forest
	mov	bx, offset stage_get_node
	retn

stage_get_node:
	mov	[byte cs:node], al
	mov	bx, offset stage_get_child1
	retn

stage_get_child1:
	mov	[cs:child1], al
	mov	bx, offset stage_get_child2_and_create_node
	retn

	; For each of the new node's values representing child nodes,
	; the low byte will be the child id. The high byte will be
	; 1 if the child is currently a leaf, and 2 otherwise.
stage_get_child2_and_create_node:
	mov	bp, [cs:node]
	shl	bp, 1
	shl	bp, 1
	mov	bl, al ; child2
	mov	bh, 0
	shl	bx, 1
	shl	bx, 1
	mov	ah, 1
	cmp	[byte ss:bx+1], 0
	jz	child2_was_a_leaf
	inc	ah

child2_was_a_leaf:
	mov	[bp+2], ax
	mov	al, [cs:child1]
	mov	bl, al
	mov	bh, 0
	shl	bx, 1
	shl	bx, 1
	mov	ah, 1
	cmp	[byte ss:bx+1], 0
	jz	child1_was_a_leaf
	inc	ah

child1_was_a_leaf:
	mov	[bp+0], ax
	dec	[cs:nodecount]
	jz	nodecount_is_zero
	mov	bx, offset stage_get_node
	retn

nodecount_is_zero:
	mov	bx, offset stage_scan_tree_from_node
	retn

	; Do a recursive scan from the node given by the input value,
	; if it differs from singletonmark. Otherwise, discard the
	; current byte, and process the following one as a decoded tree byte.
stage_scan_tree_from_node:
	cmp	al, [cs:singletonmark]
	jz	input_is_singletonmark

scan_current_node:
	mov	ah, 0
	mov	bp, ax
	shl	bp, 1
	shl	bp, 1
	mov	ax, [bp+0]
	cmp	ah, 0
	jz	process_decoded_leaf
	push	[bp+2]
	call	check_child_node ; Scan child 1
	pop	ax
	; Fallthrough: Scan child 2

check_child_node:
	cmp	ah, 1             ; 1 if the child was a leaf when the
	jnz	scan_current_node ; parent node was written, and 2 otherwise

process_decoded_leaf:
	jmp	dx

input_is_singletonmark:
	mov	bx, offset stage_process_singleton
	retn

stage_process_singleton:
	mov	bx, offset stage_scan_tree_from_node
	jmp	dx

stage_process_byte_as_is:
	jmp	dx

; Steps of processing decoded bytes

treestep_process_cmd:
	cmp	al, 0
	jz	cmd_equals_0
	cmp	al, 40h
	jb	cmd_below_40h
	cmp	al, 80h
	jb	cmd_below_80h
	jz	cmd_equals_80h
	cmp	al, 0C0h
	jb	cmd_below_C0h
	and	al, 3Fh
	mov	[byte cs:bytestowrite+1], al
	mov	dx, offset treestep_process_repeat_count_low_part
	retn

cmd_below_C0h:
	and	ax, 3Fh
	mov	[cs:bytestowrite], ax
	mov	dx, offset treestep_write_byte_repeatedly
	retn

cmd_below_80h:
	and	al, 3Fh
	mov	[byte cs:bytestowrite+1], al
	mov	dx, offset treestep_process_count_low_part
	retn

cmd_below_40h:
	and	ax, 3Fh
	mov	[cs:bytestowrite], ax
	mov	dx, offset treestep_write_byte
	retn

cmd_equals_0:
	mov	bx, offset stage_finishscan
	mov	dx, offset treestep_finishscan
	retn

cmd_equals_80h:
	mov	bx, offset stage_get_nodecount
	mov	dx, offset treestep_process_cmd
	retn

treestep_finishscan:
	retn

treestep_process_count_low_part:
	mov	[byte cs:bytestowrite], al
	mov	dx, offset treestep_write_byte
	retn

treestep_write_byte:
	stosb
	dec	[cs:bytestowrite]
	jz	count_is_zero
	retn

count_is_zero:
	mov	dx, offset treestep_process_cmd
	and	di, di
	js	next_chunk_seg
	retn

treestep_process_repeat_count_low_part:
	mov	[byte cs:bytestowrite], al
	mov	dx, offset treestep_write_byte_repeatedly
	retn

treestep_write_byte_repeatedly:
	push	cx
	mov	cx, [cs:bytestowrite]
	repe stosb
	pop	cx
	mov	dx, offset treestep_process_cmd
	and	di, di
	js	next_chunk_seg
	retn

	; Update output segment and offset after writing at least 8000h bytes.
	; The actual location isn't changed, it's just that the address
	; is modified to have a lower offset.
next_chunk_seg:
	sub	di, 8000h
	mov	ax, es
	add	ax, 800h
	mov	es, ax
	retn

	; Try to read cx bytes from file bx to the buffer at ax:dx
read_from_file:
	push	bx
	push	ds
	mov	ds, ax
	mov	ax, 3F00h
	int	21h
	pop	ds
	pop	bx
	mov	cx, ax ; Actual amount of bytes read
	jb	abort
	retn

	; Write error message about DAT file access, and then exit with code 1
abort:
	push	cs
	pop	ds
	mov	dx, offset errormsg
	mov	cx, [path_end_offset]
	sub	cx, dx
	mov	bx, 1
	mov	ah, 40h
	int	21h
	mov	ax, 4C01h
	int	21h


prefixedfile:

IFDEF DESERT
	db '\DESERT.DAT', 0
ELSEIFDEF JUNGLE
	db '\JUNGLE.DAT', 0
ENDIF

errormsg:
	db 'Error accessing file '
filewithpath:
	db 80h dup(0)
path_end_offset		dw 0
orig_seg_ptr		dw loader
file_chunk_seg_ptr	dw ?

psp_seg	dw ?

	dw ?

file_handle	dw ?

dat_header:		db 20h dup(?)
tree:			db 400h dup(?)
exe_header_and_reloc:	db EXE_HEADER_AND_RELOC_BUFFER_SIZE+1000h dup(?)

end_of_data:

stack 0FFFFh

END
