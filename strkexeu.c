/*
Copyright (c) 2021, NY00123
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. The name of the author may not be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#if __STDC_VERSION__ >= 199901L
#include <stdbool.h>
#include <stdint.h>
#else
typedef enum {false, true} bool;
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
#endif

static FILE *in, *out;
static bool done = false;

static bool tree_parse_next_command(uint8_t ch);

static bool (*tree_byte_command_ptr)(uint8_t) = tree_parse_next_command;
static uint16_t output_char_count;

static uint8_t read_byte(void)
{
	int ch = fgetc(in);
	if (ch == EOF)
	{
		fputs("File read failure and/or unexpected EOF!", stderr);
		abort();
	}
	return ch;
}

static void write_byte(uint8_t ch)
{
	if (fputc(ch, out) != ch)
	{
		fputs("File write failure!", stderr);
		abort();
	}
}

static bool tree_write_char(uint8_t ch)
{
	write_byte(ch);
	if (--output_char_count == 0)
		tree_byte_command_ptr = tree_parse_next_command;
	return true;
}

static bool tree_parse_char_count_hi_part(uint8_t ch)
{
	output_char_count &= 0xFF00;
	output_char_count |= ch;
	tree_byte_command_ptr = tree_write_char;
	return true;
}

static bool tree_write_char_repeatedly(uint8_t ch)
{
	while (output_char_count--)
		write_byte(ch);
	tree_byte_command_ptr = tree_parse_next_command;
	return true;
}

static bool tree_parse_char_repeat_count_hi_part(uint8_t ch)
{
	output_char_count &= 0xFF00;
	output_char_count |= ch;
	tree_byte_command_ptr = tree_write_char_repeatedly;
	return true;
}

static bool tree_parse_next_command(uint8_t ch)
{
	if (ch == 0)
	{
		done = true;
		return false;
	}
	else if (ch < 64)
	{
		output_char_count = ch & 63;
		tree_byte_command_ptr = tree_write_char;
	}
	else if (ch < 128)
	{
		output_char_count = (ch & 63) << 8;
		tree_byte_command_ptr = tree_parse_char_count_hi_part;
	}
	else if (ch == 128)
	{
		return false;
	}
	else if (ch < 192)
	{
		output_char_count = ch & 63;
		tree_byte_command_ptr = tree_write_char_repeatedly;
	}
	else
	{
		output_char_count = (ch & 63) << 8;
		tree_byte_command_ptr = tree_parse_char_repeat_count_hi_part;
	}
	return true;
}

/* Tree encoding:
 * - A leaf is set to {0, 0}.
 * - If a newly created internal node points to child i,
 * the corresponding child field is set to i+1 or -i-1.
 * In case i was a leaf at the point of creating
 * the node, i+1 is used. Otherwise, -i-1 is used.
 */
typedef struct
{
	int child1, child2;
} node_t;

typedef node_t tree_t[256];

static void init_tree(tree_t *tree)
{
	int i = 0;
	for (i = 0; i < 256; ++i)
		(*tree)[i].child1 = (*tree)[i].child2 = 0;
}

static bool is_leaf(const tree_t *tree, int node)
{
	return (*tree)[node].child1 == 0;
}

static void set_node_children(tree_t *tree, int node, int child1, int child2)
{
	assert(node != child1);
	assert(node != child2);
	assert(is_leaf(tree, node));

	(*tree)[node].child1 = is_leaf(tree, child1) ? child1+1 : -child1-1;
	(*tree)[node].child2 = is_leaf(tree, child2) ? child2+1 : -child2-1;
}

static void seek_dat_to_exe(void)
{
	uint8_t buffer[32];
	uint16_t offset;
	if (fread(buffer, sizeof(buffer), 1, in) != 1)
	{
		fputs("Reading header from data file failed!", stderr);
		abort();
	}
	offset = buffer[24] | (buffer[25] << 8);
	if (fseek(in, offset, SEEK_SET) != 0)
	{
		fputs("Seeking to internal exe in data file failed!", stderr);
		abort();
	}
}

static bool scan_tree(const tree_t *tree, int node)
{
	int c1, c2;
	bool ret1, ret2;
	if (is_leaf(tree, node))
		return tree_byte_command_ptr(node);
	c1 = (*tree)[node].child1;
	c2 = (*tree)[node].child2;
	ret1 = (c1 < 0) ? scan_tree(tree, -c1-1) : tree_byte_command_ptr(c1-1);
	ret2 = (c2 < 0) ? scan_tree(tree, -c2-1) : tree_byte_command_ptr(c2-1);
	return ret1 && ret2;
}

static void unpack(void)
{
	tree_t tree;
	int nodecount = read_byte();
	int treesingletonmark = read_byte();

	if (nodecount == 0)
	{
		while (tree_byte_command_ptr(read_byte()))
			;
		return;
	}

	init_tree(&tree);
	while (nodecount--)
	{
		int node = read_byte();
		int child1 = read_byte();
		int child2 = read_byte();
		set_node_children(&tree, node, child1, child2);
	}

	while (1)
	{
		int node = read_byte();
		if (node == treesingletonmark)
		{
			if (!tree_byte_command_ptr(read_byte()))
				return;
			continue;
		}
		if (!scan_tree(&tree, node))
			break;
	}
}

int main(int argc, char **argv)
{
	puts("");
	puts("strkexeu v0.1");
	puts("-------------");
	puts("");

	if (argc != 3)
	{
		puts("Usage: strkexeu <IN_DAT> <OUT_EXE>");
		puts("");
		return 0;
	}
	if (!(in = fopen(argv[1], "rb")))
	{
		fputs("Failed to open input file for reading!", stderr);
		return 1;
	}
	seek_dat_to_exe();
	if (!(out = fopen(argv[2], "wb")))
	{
		fputs("Failed to open output file for writing!", stderr);
		fclose(in);
		return 2;
	}

	puts("Unpacking the exe...");
	while (!done)
		unpack();

	fclose(out);
	fclose(in);
	puts("The exe has been unpacked.");
	return 0;
}
